package cardGame;

import javafx.fxml.FXML;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

/**
 * @author markuol
 */

public class DeckOfCards {

    private final List<PlayingCard> cardsDeck = new ArrayList<>();


    /**
     * Creates a full deck of 52 playingcards
     */
    public DeckOfCards() {
        for (int i = 0; i < PlayingCard.RANK.length; i++) {
            for (int j = 0; j < PlayingCard.SUIT.length; j++) {
                cardsDeck.add(new PlayingCard(PlayingCard.SUIT[j],PlayingCard.RANK[i]));
            }
        }
    }


    /**
     * deals a hand of n number of random cards. Does not remove the chosen cards from the deck.
     * @param n number of cards
     * @return a list containing n-number of randomly chosen cards
     */
    public List<PlayingCard> dealHand(int n) {
        if (n < 1 || n > cardsDeck.size()) {
            throw new IllegalArgumentException("Illegal input. Please make sure the input is between 1 and 52");
        }
        Random random = new Random();
        List<PlayingCard> handOfCards = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            int cardIndex = random.nextInt(cardsDeck.size());
            handOfCards.add(cardsDeck.get(cardIndex));
        }
        return handOfCards;
    }


    @Override
    public String toString() {
        return "cardGame.DeckOfCards{" +
                "cardsDeck=" + cardsDeck +
                '}';
    }

    /**
     *
     * @return  a stream mapping to the faces of all playingcards, then adding the sum together
     */
    public int sumOfHand() {
        return cardsDeck.stream().mapToInt(PlayingCard::getFace).sum();
    }

    /**
     *
     * @param handWithCards
     * @return filters a hand of cards to find all cards of suit 'H', before returning it to a list
     */
    public List<PlayingCard> suitOfHand(List<PlayingCard> handWithCards) {
        return handWithCards.stream().filter(card -> card.getSuit()=='H').toList();
    }

    /**
     *
     * @param handWithCards
     * @return A stream checking whether a hand of cards contains the spadesuit and the face 12, which is
     * corresponding to the queen.
     */
    public boolean queenOfSpadesCheck(List<PlayingCard> handWithCards) {
        return handWithCards.stream().anyMatch(card -> card.getSuit() == 'S' && card.getFace() == 12);
    }

    /**
     *
     * @param handWithCards
     * @return not sure how to make this one work.
     */
    public boolean flushCheck(List<PlayingCard> handWithCards) {
        return handWithCards.stream().mapToInt(PlayingCard::getSuit)
    }

}
